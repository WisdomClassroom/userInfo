/*******************************************************************************
 * Copyright 2020 huanggefan.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package service

import (
	"gitee.com/WisdomClassroom/core"
	"gitee.com/WisdomClassroom/core/protobufs/v1/pb"
)

type Service struct{}

var loginRespSvrErr = &pb.LoginResponse{Status: &pb.ResponseStatus{Code: core.ResponseStatusCodeServerError}}
var listUsersRespSvrErr = &pb.ListUsersResponse{Status: &pb.ResponseStatus{Code: core.ResponseStatusCodeServerError}}
var createUsersRespSvrErr = &pb.CreateUsersResponse{Status: &pb.ResponseStatus{Code: core.ResponseStatusCodeServerError}}
var updateInfoRespSvrErr = &pb.UpdateInfoResponse{Status: &pb.ResponseStatus{Code: core.ResponseStatusCodeServerError}}
var updatePasswordRespSvrErr = &pb.UpdatePasswordResponse{Status: &pb.ResponseStatus{Code: core.ResponseStatusCodeServerError}}
var resetPasswordRespSvrErr = &pb.ResetPasswordResponse{Status: &pb.ResponseStatus{Code: core.ResponseStatusCodeServerError}}
var deleteUserRespSvrErr = &pb.DeleteUserResponse{Status: &pb.ResponseStatus{Code: core.ResponseStatusCodeServerError}}
var setUserAsManagerRespSvrErr = &pb.SetUserAsManagerResponse{Status: &pb.ResponseStatus{Code: core.ResponseStatusCodeServerError}}
