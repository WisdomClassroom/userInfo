package service

import (
	"context"

	"gitee.com/WisdomClassroom/core"
	"gitee.com/WisdomClassroom/core/models"
	"gitee.com/WisdomClassroom/core/protobufs/v1/pb"
	"gitee.com/WisdomClassroom/userInfo/glb"
	"google.golang.org/grpc/metadata"
	"gorm.io/gorm"
)

func (s *Service) ResetPassword(ctx context.Context, request *pb.ResetPasswordRequest) (*pb.ResetPasswordResponse, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		glb.Logger.Error("no metadata")
	}
	token, err := core.UnpackTokenFromMetadata(md)
	if err != nil {
		return &pb.ResetPasswordResponse{Status: &pb.ResponseStatus{
			Code: core.ResponseStatusCodeNotAuth, Message: err.Error(),
		}}, nil
	}

	if !token.IsManager {
		return &pb.ResetPasswordResponse{Status: &pb.ResponseStatus{
			Code:    core.ResponseStatusCodeNotAuth,
			Message: "用户没有权限",
		}}, nil
	}

	user := &models.User{AccountID: request.GetAccountID()}
	err = glb.DB.Select("account_id").Take(user).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return &pb.ResetPasswordResponse{Status: &pb.ResponseStatus{
				Code:    core.ResponseStatusCodeOtherError,
				Message: "用户不存在",
			}}, nil
		}
		glb.Logger.Error(err.Error())
		return resetPasswordRespSvrErr, nil
	}

	user.SetPassword(core.StringToMD5(core.DefaultPassword))
	err = glb.DB.Model(user).Updates(user).Error
	if err != nil {
		glb.Logger.Error(err.Error())
		return resetPasswordRespSvrErr, nil
	}

	return &pb.ResetPasswordResponse{
		Status: &pb.ResponseStatus{Code: core.ResponseStatusCodeSuccess},
	}, nil
}
