module gitee.com/WisdomClassroom/userInfo

go 1.15

require (
	gitee.com/WisdomClassroom/core v0.7.27
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.3.0
	google.golang.org/grpc v1.36.1
	gorm.io/driver/postgres v1.0.8
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.6
)
