# Copyright 2020 huanggefan.cn
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.PYONY: all, test, clean, sync

SHELL=/bin/bash

BUILD_TAG=$(shell git describe --always)
BUILD_TIME=$(shell date +%F@%T)
BUILD_GO=$(shell go version)
EXEC_FILE=service-user

LDFLAGS = -ldflags '-X "main.BuildTag=${BUILD_TAG}" -X "main.BuildGo=${BUILD_GO}" -X "main.BuildTime=${BUILD_TIME}"'

all: test
	go build -o ${EXEC_FILE} ${LDFLAGS} .

test: clean
	@go test -cover -covermode=count -coverprofile=coverage.out .
	@go tool cover -func=coverage.out
	#@go tool cover -html=coverage.out

clean:
	@rm -vf coverage.out
	@rm -vf ${EXEC_FILE}

sync:
	go mod tidy

run:
	./${EXEC_FILE} -log 0 -use-pg -pg-addr 127.0.0.1 -pg-port 5432 -pg-db postgres -pg-user postgres -pg-password postgres
